package interfaz;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import logica.Persona;

public class Personas extends JPanel {

	private static final long serialVersionUID = 1L;

	private static final String[] NOMBRE_COLUMNAS = { "NOMBRE", "INTERES DEPORTE", "INTERES MUSICA", "INTERES NOTICIAS",
			"INTERES CIENCIA" };

	private JPanel panelSuperior;
	private JButton btnAgregar;

	private JScrollPane scrollPane;
	private DefaultTableModel modelTablaPersona;
	private JTable tablaPersonas;

	private Formulario formulario;
	
	private ArrayList<Persona> personas;

	public Personas() {
		personas = new ArrayList<>();

		formulario = new Formulario();
		formulario.setActionAgregar((a) -> agregarPersona(a));

		setBounds(100, 100, 650, 400);
		setLayout(new BorderLayout(5, 5));

		panelSuperior = new JPanel();
		add(panelSuperior, BorderLayout.NORTH);

		btnAgregar = new JButton("Agregar Persona");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formulario.limpiarFomulario();
				formulario.abrir();
			}
		});
		panelSuperior.add(btnAgregar);

		scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);

		modelTablaPersona = new DefaultTableModel(null, NOMBRE_COLUMNAS);
		tablaPersonas = new JTable(modelTablaPersona);
		scrollPane.setViewportView(tablaPersonas);
	}

	public void agregarPersona(ActionEvent a) {
		Persona nuevaPersona = formulario.getDatosPersona();
		personas.add(nuevaPersona);

		actualizarTabla();

		formulario.cerrar();
	}

	private void actualizarTabla() {
		limpiarTabla();

		for (Persona persona : personas) {
			Object[] fila = { persona.getNombre(), persona.getInteresDeportes(), persona.getInteresMusica(),
					persona.getInteresEspectaculo(), persona.getInteresCiencia() };
			agregarFila(fila);
		}
	}

	private void limpiarTabla() {
		modelTablaPersona.setRowCount(0);
		modelTablaPersona.setColumnCount(0);
		modelTablaPersona.setColumnIdentifiers(NOMBRE_COLUMNAS);
	}

	private void agregarFila(Object[] fila) {
		modelTablaPersona.addRow(fila);
	}

}
