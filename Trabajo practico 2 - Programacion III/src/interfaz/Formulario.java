package interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.MatteBorder;

import logica.Persona;

public class Formulario extends JDialog {

	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	private final int MINIMO = 1;
	private final int MAXIMO = 5;

	private JTextField textNombre;
	private JSpinner spinnerDeporte;
	private JSpinner spinnerMusica;
	private JSpinner spinnerNoticias;
	private JSpinner spinnerCiencia;
	private JButton btnAgregar;

	public Formulario() {
		setBounds(100, 100, 370, 365);
		setTitle("Agregar Persona");
		getContentPane().setLayout(new BorderLayout(5, 5));
		contentPanel.setBorder(new MatteBorder(2, 2, 2, 2, (Color) Color.LIGHT_GRAY));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);

		JLabel lblNombre = new JLabel("Nombre y Apellido");
		lblNombre.setBounds(10, 39, 163, 14);
		contentPanel.add(lblNombre);

		textNombre = new JTextField();
		textNombre.setBounds(198, 39, 146, 20);
		contentPanel.add(textNombre);
		textNombre.setColumns(10);

		JLabel lblDeporte = new JLabel("Interés en Deporte");
		lblDeporte.setBounds(10, 90, 163, 14);
		contentPanel.add(lblDeporte);

		spinnerDeporte = new JSpinner(new SpinnerNumberModel(MINIMO, MINIMO, MAXIMO, 1));
		spinnerDeporte.setOpaque(false);
		spinnerDeporte.setBounds(245, 90, 99, 20);
		contentPanel.add(spinnerDeporte);

		JLabel lblMusica = new JLabel("Interés en Música");
		lblMusica.setBounds(10, 141, 163, 14);
		contentPanel.add(lblMusica);

		spinnerMusica = new JSpinner(new SpinnerNumberModel(MINIMO, MINIMO, MAXIMO, 1));
		spinnerMusica.setBounds(245, 141, 99, 20);
		contentPanel.add(spinnerMusica);

		JLabel lblNoticas = new JLabel("Interés en Noticas de Espectáculo");
		lblNoticas.setBounds(10, 192, 225, 14);
		contentPanel.add(lblNoticas);

		spinnerNoticias = new JSpinner(new SpinnerNumberModel(MINIMO, MINIMO, MAXIMO, 1));
		spinnerNoticias.setBounds(245, 192, 99, 20);
		contentPanel.add(spinnerNoticias);

		JLabel lblCiencia = new JLabel("Interés en Ciencia");
		lblCiencia.setBounds(10, 243, 163, 14);
		contentPanel.add(lblCiencia);

		spinnerCiencia = new JSpinner(new SpinnerNumberModel(MINIMO, MINIMO, MAXIMO, 1));
		spinnerCiencia.setBounds(245, 243, 99, 20);
		contentPanel.add(spinnerCiencia);

		btnAgregar = new JButton("AGREGAR");
		btnAgregar.setBounds(245, 294, 99, 23);
		contentPanel.add(btnAgregar);

	}
	
	public void setActionAgregar(ActionListener listener){
		this.btnAgregar.addActionListener(listener);
	}
	
	public Persona getDatosPersona() {
		Integer valorDeporte = Integer.parseInt(spinnerDeporte.getValue().toString());
		Integer valorMusica = Integer.parseInt(spinnerMusica.getValue().toString());
		Integer valorNoticas = Integer.parseInt(spinnerNoticias.getValue().toString());
		Integer valorCiencia = Integer.parseInt(spinnerCiencia.getValue().toString());
		
		return new Persona(textNombre.getText(), valorDeporte, valorMusica,
				valorNoticas, valorCiencia);
	}

	public JTextField getTextNombre() {
		return textNombre;
	}

	public void setTextNombre(JTextField textNombre) {
		this.textNombre = textNombre;
	}

	public JSpinner getSpinnerDeporte() {
		return spinnerDeporte;
	}

	public void setSpinnerDeporte(JSpinner spinnerDeporte) {
		this.spinnerDeporte = spinnerDeporte;
	}

	public JSpinner getSpinnerMusica() {
		return spinnerMusica;
	}

	public void setSpinnerMusica(JSpinner spinnerMusica) {
		this.spinnerMusica = spinnerMusica;
	}

	public JSpinner getSpinnerNoticias() {
		return spinnerNoticias;
	}

	public void setSpinnerNoticias(JSpinner spinnerNoticias) {
		this.spinnerNoticias = spinnerNoticias;
	}

	public JSpinner getSpinnerCiencia() {
		return spinnerCiencia;
	}

	public void setSpinnerCiencia(JSpinner spinnerCiencia) {
		this.spinnerCiencia = spinnerCiencia;
	}

	public void limpiarFomulario() {
		getTextNombre().setText(null);
		getSpinnerDeporte().setValue(MINIMO);
		getSpinnerMusica().setValue(MINIMO);
		getSpinnerNoticias().setValue(MINIMO);
		getSpinnerCiencia().setValue(MINIMO);
	}
	
	public void abrir() {
		setVisible(true);
	}

	public void cerrar() {
		dispose();
	}

}
