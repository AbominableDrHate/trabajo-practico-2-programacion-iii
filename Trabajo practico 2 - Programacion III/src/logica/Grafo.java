package logica;

import java.util.ArrayList;

public class Grafo {
	private ArrayList<Persona> vertices;
	private ArrayList<Arista> aristas;
	
	public Grafo(ArrayList<Persona> personas) {
		this.aristas=new ArrayList<Arista>();
		this.vertices=personas;
		for(int i=0;i<personas.size();i++) {
			for(int j=0;j<personas.size();j++) {
				if(i!=j) {
					Arista arista = new Arista(personas.get(i),personas.get(j));
					aristas.add(arista);
				}
			}
		}
			
	
	}

	public ArrayList<Persona> getVertices() {
		return vertices;
	}

	public ArrayList<Arista> getAristas() {
		return aristas;
	}
	
	public int cantidadDeVertices() {
		return this.vertices.size();
	}
	
	public int cantidadDeAristas() {
		return this.aristas.size();
	}
	
	
}
