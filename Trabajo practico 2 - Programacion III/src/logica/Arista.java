package logica;

public class Arista {
	private Persona vertice1;
	private Persona vertice2;
	private int peso;

	public Arista(Persona vertice1,Persona vertice2) {
		this.vertice1=vertice1;
		this.vertice2=vertice2;
		this.peso=(Math.abs(this.vertice1.getInteresDeportes()-vertice2.getInteresDeportes())+
				Math.abs(this.vertice1.getInteresMusica()-vertice2.getInteresMusica())+
				Math.abs(this.vertice1.getInteresEspectaculo()-vertice2.getInteresEspectaculo())+
				Math.abs(this.vertice1.getInteresCiencia()-vertice2.getInteresCiencia()));
	}

	@Override
	public String toString() {
		return "Arista [Persona1=" + vertice1.getNombre() + ", Persona2=" + vertice2.getNombre() + ", peso=" + peso + "]";
	}


	public int getPeso() {
		return peso;
	}

	public Persona getVertice1() {
		return vertice1;
	}

	public Persona getVertice2() {
		return vertice2;
	}
}
