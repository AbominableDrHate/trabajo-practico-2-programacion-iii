package logica;

public class Persona {
	private String nombre;
	private int deportes;
	private int musica;
	private int espectaculo;
	private int ciencia;
	
	public Persona(String nombre,int deportes, int musica, int espectaculo, int ciencia) {
		this.nombre = nombre;
		this.deportes = deportes;
		this.musica = musica;
		this.espectaculo = espectaculo;
		this.ciencia = ciencia;
	}
	
	@Override
	public String toString() {
		return "Nombre=" + nombre;
	}

	public int getInteresDeportes() {
		return deportes;
	}

	public int getInteresMusica() {
		return musica;
	}

	public int getInteresEspectaculo() {
		return espectaculo;
	}

	public int getInteresCiencia() {
		return ciencia;
	}

	public String getNombre() {
		return nombre;
	}
	
	
	
	
}
