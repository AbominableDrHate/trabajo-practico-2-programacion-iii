package logica;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class GrafoTest {

	@Test
	public void testCantidadAristas() {
		ArrayList<Persona> personas = new ArrayList<Persona>();
		Persona pepe = new Persona("Pepe", 5, 5, 5, 5);
		Persona juan = new Persona("Juan", 5, 2, 4, 5);
		Persona octavio = new Persona("Octavio", 3, 2, 5, 5);
		personas.add(pepe);
		personas.add(juan);
		personas.add(octavio);
		Grafo grafo = new Grafo(personas);
		assert true: (grafo.cantidadDeAristas()==3);
	}
	
	@Test
	public void testCantidadVertices() {
		ArrayList<Persona> personas = new ArrayList<Persona>();
		Persona pepe = new Persona("Pepe", 5, 5, 5, 5);
		Persona juan = new Persona("Juan", 5, 2, 4, 5);
		Persona octavio = new Persona("Octavio", 3, 2, 5, 5);
		personas.add(pepe);
		personas.add(juan);
		personas.add(octavio);
		Grafo grafo = new Grafo(personas);
		assert true: (grafo.cantidadDeVertices()==3);
		
	}

}
